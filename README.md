# MPlus Compiler

The compiler for MPlus targeting the MPVM virtual machine

## Features missing

The unary `&` for quoting

## Build Instructions

You need a Java 8 (or above) compiler.

Run `gradlew build`, and it will be built in `build/distributions`.

## Use Instructions

```
mplus_compiler file_name
```
