/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class StringData implements Data {

    public final String val;

    public StringData(String val) {
        this.val = val;
    }

    @Override
    public String toSExpr() {
        return val;
    }

    @Override
    public String toASM() {
        return String.format("    ldc.str %s", val);
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, MutableInt addr) throws IOException {
        final int slot = Utils.newConstant(k, val);
        Utils.writeOpcode_i(os, addr, Opcodes.LDC_STR, slot);
    }
}
