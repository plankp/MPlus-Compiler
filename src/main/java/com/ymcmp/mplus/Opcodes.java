/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

/**
 *
 * @author YTENG
 */
public class Opcodes {

    public static final byte LDB = 0;
    public static final byte LDI = 1;
    public static final byte LDC_I8 = 2;
    public static final byte LDC_I16 = 3;
    public static final byte LDC_I32 = 4;
    public static final byte LDC_I64 = 5;
    public static final byte LDC_F32 = 6;
    public static final byte LDC_F64 = 7;
    public static final byte LDC_STR = 8;
    public static final byte LDC_NIL = 9;
    public static final byte LDC_ARRAY = 10;
    public static final byte STB = 11;
    public static final byte STI = 12;
    public static final byte DECLB = 13;
    public static final byte DECLI = 14;
    public static final byte UCALL = 15;
    public static final byte CALL = 16;
    public static final byte RET = 17;
    public static final byte YIELD = 18;
    public static final byte JMP = 19;
    public static final byte JMP_TRUE = 20;
    public static final byte JMP_FALSE = 21;
    public static final byte DUP = 22;
    public static final byte POP = 23;
    public static final byte SWP = 24;
    public static final byte FUNC_LOAD = 25;
    public static final byte FUNC_MAP = 26;
    public static final byte FUNC_OMAP = 27;
    public static final byte FUNC_VARARGS = 28;
    public static final byte LAZY_LOAD = 29;
    public static final byte ERR_THROW = 30;
    public static final byte ERR_HANDLE = 31;
    public static final byte ERR_RETHROW = 32;
    public static final byte ERR_DECL = 33;
    public static final byte PRINT = 34;
    public static final byte LD_ADD = 35;
    public static final byte LD_SUB = 36;
    public static final byte LD_MUL = 37;
    public static final byte LD_DIV = 38;
    public static final byte LD_MOD = 39;
    public static final byte LD_POW = 40;
    public static final byte LD_PERCENT = 41;
    public static final byte LD_CONS = 42;
    public static final byte LD_GT = 43;
    public static final byte LD_LT = 44;
    public static final byte LD_GE = 45;
    public static final byte LD_LE = 46;
    public static final byte LD_EQL = 47;
    public static final byte LD_NEQ = 48;
    public static final byte LD_NOT = 49;
    public static final byte LD_AND = 50;
    public static final byte LD_OR = 51;
    public static final byte LD_FCMP = 52;
}
