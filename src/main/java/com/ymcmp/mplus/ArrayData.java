/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class ArrayData implements Data {

    public final Data[] val;

    public ArrayData(Data... val) {
        this.val = val;
    }

    @Override
    public String toSExpr() {
        if (val.length == 0) {
            return "[]";
        }

        Data data = new ArrayData();
        for (int i = val.length; i > 0; --i) {
            data = new FCall(new AtomData(":"), val[i - 1], data);
        }
        return data.toSExpr();
    }

    @Override
    public String toASM() {
        if (val.length == 0) {
            return "    ldc.array 0";
        }

        final StringBuilder sb = new StringBuilder();
        for (int i = val.length; i > 0; --i) {
            sb.append(val[i - 1].toASM()).append('\n');
        }
        sb.append("    ldc.array ").append(val.length);
        return sb.toString();
    }

    @Override
    public void toBytecode(final java.io.OutputStream os, final Map<String, Integer> k, final MutableInt addr) throws IOException {
	if (val.length == 0) {
	    Utils.writeOpcode_i(os, addr, Opcodes.LDC_ARRAY, 0);
	}

	for (int i = val.length; i > 0; --i) {
	    val[i - 1].toBytecode(os, k, addr);
	}
	Utils.writeOpcode_i(os, addr, Opcodes.LDC_ARRAY, val.length);
    }
}
