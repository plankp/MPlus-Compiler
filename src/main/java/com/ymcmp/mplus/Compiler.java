package com.ymcmp.mplus;

import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;

import java.nio.ByteBuffer;

import java.util.Arrays;
import java.util.HashMap;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

/**
 *
 * @author YTENG
 */
public class Compiler {

    public static String makeBytecodeExt(String filename) {
	if (filename.contains(".")) {
	    filename = filename.substring(0, filename.lastIndexOf('.'));
	}
	return filename + ".mpbc";
    }
    
    public static void main(String[] args) throws java.io.IOException {
	if (args.length != 1) {
	    System.err.println("Expected file name as argument");
	    return;
	}
       
	CharStream stream = CharStreams.fromFileName(args[0]);
        GrammarLexer lexer = new GrammarLexer(stream);
        TokenStream toks = new CommonTokenStream(lexer);
        GrammarParser parser = new GrammarParser(toks);

        final Data data = new SexpGenerator().visit(parser.exprs()).getData();
        System.out.println("SExpr:\n" + data.toSExpr());
        System.out.println("ASM:\n" + data.toASM());

        final HashMap<String, Integer> kmap = new HashMap<>();
        try (final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            data.toBytecode(baos, kmap, new MutableInt());
            baos.flush();

	    try (final FileOutputStream fos = new FileOutputStream(makeBytecodeExt(args[0]))) {
		final byte[] code = baos.toByteArray();

		final ByteBuffer buf = ByteBuffer.allocate(12);
		buf.put((byte) 0xBC).put((byte) 0x14)
		    .put((byte) 0x70).put((byte) 0x01)
		    .putInt(kmap.size())
		    .putInt(code.length);
		fos.write(buf.array());
		
		kmap.entrySet().stream()
                    .sorted((a, b) -> a.getValue().compareTo(b.getValue()))
                    .forEach(el -> {
			    try {
				final byte[] k = el.getKey().getBytes();
				final ByteBuffer ibuf = ByteBuffer.allocate(4);
				ibuf.putInt(k.length);
				fos.write(ibuf.array());
				fos.write(k);
			    } catch (java.io.IOException ex) {
				throw new RuntimeException(ex);
			    }
			});

		fos.write(code);
	    }
        }
    }
}
