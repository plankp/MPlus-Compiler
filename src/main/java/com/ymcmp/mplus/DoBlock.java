/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class DoBlock implements Data {

    public final Data[] val;

    public DoBlock(Data... val) {
        this.val = val;
    }

    @Override
    public String toSExpr() {
        final StringBuilder sb = new StringBuilder("(do");

        for (final Data data : val) {
            sb.append(' ').append(data.toSExpr());
        }
        return sb.append(')').toString();
    }

    @Override
    public String toASM() {
        final StringBuilder sb = new StringBuilder();
        for (final Data data : val) {
            sb.append(data.toASM()).append('\n');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, final MutableInt addr) throws IOException {
        for (final Data data : val) {
            data.toBytecode(os, k, addr);
        }
    }
}
