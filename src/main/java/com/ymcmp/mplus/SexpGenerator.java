package com.ymcmp.mplus;

/**
 *
 * @author YTENG
 */
public class SexpGenerator extends GrammarBaseVisitor<SexpGenerator.Variant> {

    public static final class Variant {

        public final Object obj;
        public final boolean isData;

        private Variant(Object obj, boolean isData) {
            this.obj = obj;
            this.isData = isData;
        }

        public boolean isData() {
            return isData;
        }

        public Data getData() {
            return (Data) obj;
        }

        private static Variant from(Data data) {
            return new Variant(data, true);
        }

        private static Variant from(Object data) {
            return new Variant(data, false);
        }
    }

    @Override
    public Variant visitExprs(GrammarParser.ExprsContext ctx) {
        if (ctx.getChildCount() < 3) {
            return visit(ctx.getChild(0));
        }

        final Data[] datum = new Data[(ctx.getChildCount() + 1) / 2];
        for (int i = 0; i < ctx.getChildCount(); i += 2) {
            datum[i / 2] = visit(ctx.getChild(i)).getData();
        }
        return Variant.from(new DoBlock(datum));
    }

    @Override
    public Variant visitPrimDelegateDo(GrammarParser.PrimDelegateDoContext ctx) {
        return visit(ctx.getChild(1));
    }

    @Override
    public Variant visitInnerArgs(GrammarParser.InnerArgsContext ctx) {
        final Data[] datum = new Data[(ctx.getChildCount() + 1) / 2];
        for (int i = 0; i < ctx.getChildCount(); i += 2) {
            datum[i / 2] = visit(ctx.getChild(i)).getData();
        }
        return Variant.from(datum);
    }

    @Override
    public Variant visitArgs(GrammarParser.ArgsContext ctx) {
        if (ctx.getChildCount() == 2) {
            return Variant.from(new Data[0]);
        }
        return visit(ctx.getChild(1));
    }

    @Override
    public Variant visitPrimNil(GrammarParser.PrimNilContext ctx) {
        return Variant.from(NilData.INSTANCE);
    }

    @Override
    public Variant visitPrimBraced(GrammarParser.PrimBracedContext ctx) {
        return visit(ctx.getChild(1));
    }

    @Override
    public Variant visitCases(GrammarParser.CasesContext ctx) {
        switch (ctx.getChildCount()) {
        case 2:
            return Variant.from(new Object[]{visit(ctx.getChild(0)).getData()});
        case 3:
            return Variant.from(new Object[]{
                new CondBlock.Case(visit(ctx.getChild(2)).getData(),
                                   visit(ctx.getChild(0)).getData())});
        case 5:
            final CondBlock.Case c = new CondBlock.Case(visit(ctx.getChild(2)).getData(),
                                                        visit(ctx.getChild(0)).getData());
            final Object[] v = (Object[]) visit(ctx.getChild(4)).obj;
            final Object[] p = new Object[v.length + 1];
            p[0] = c;
            System.arraycopy(v, 0, p, 1, v.length);
            return Variant.from(p);
        }
        throw new AssertionError("This shouldnt happen -- cases");
    }

    @Override
    public Variant visitParam(GrammarParser.ParamContext ctx) {
        if (ctx.getChildCount() == 1) {
            return Variant.from(new Lambda.Param(ctx.getChild(0).getText()));
        }
        return Variant.from(new Lambda.Param(ctx.getChild(0).getText(),
                                             visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitParams(GrammarParser.ParamsContext ctx) {
        primF2VarargFlag = false;
        switch (ctx.getChildCount()) {
        case 1:
            return Variant.from(new Lambda.Param[]{(Lambda.Param) visit(ctx.getChild(0)).obj});
        case 2:
            primF2VarargFlag = true;
            return Variant.from(new Lambda.Param[]{(Lambda.Param) visit(ctx.getChild(1)).obj});
        case 3:
            final Lambda.Param[] arr = (Lambda.Param[]) visit(ctx.getChild(2)).obj;
            final Lambda.Param[] p = new Lambda.Param[arr.length + 1];
            p[0] = (Lambda.Param) visit(ctx.getChild(0)).obj;
            System.arraycopy(arr, 0, p, 1, arr.length);
            return Variant.from(p);
        }
        throw new AssertionError("This should not happen -- params");
    }

    @Override
    public Variant visitPrimVal(GrammarParser.PrimValContext ctx) {
        final String text = ctx.getText();
        if (text.charAt(0) == '@') {
            return Variant.from(new StringData(text));
        }
        if (text.charAt(0) == '0') {
            if (text.length() == 1) {
                return Variant.from(new IntData(0));
            }
            switch (text.charAt(1)) {
            case 'b':
                return Variant.from(new IntData(Long.parseLong(text.substring(2), 2)));
            case 'c':
                return Variant.from(new IntData(Long.parseLong(text.substring(2), 8)));
            case 'd':
                return Variant.from(new IntData(Long.parseLong(text.substring(2), 10)));
            case 'x':
                return Variant.from(new IntData(Long.parseLong(text.substring(2), 16)));
            default:
                throw new AssertionError("This should not happen -- prim::int");
            }
        }
        if (Character.isDigit(text.charAt(0))) {
            if (text.contains(".")) {
                return Variant.from(new FloatData(Double.parseDouble(text)));
            }
            return Variant.from(new IntData(Long.parseLong(text)));
        }
        return Variant.from(new AtomData(text));
    }

    @Override
    public Variant visitPrimArray(GrammarParser.PrimArrayContext ctx) {
        if (ctx.getChildCount() == 2) {
            return Variant.from(new ArrayData());
        }
        return Variant.from(new ArrayData((Data[]) visit(ctx.getChild(1)).obj));
    }

    @Override
    public Variant visitPrimCond(GrammarParser.PrimCondContext ctx) {
        final Object[] cs = (Object[]) visit(ctx.getChild(1)).obj;
        if (cs[cs.length - 1] instanceof Data) {
            if (cs.length == 1) {
                return Variant.from(new CondBlock((Data) cs[cs.length - 1]));
            }
            final CondBlock.Case[] p = new CondBlock.Case[cs.length - 1];
            System.arraycopy(cs, 0, p, 0, p.length);
            return Variant.from(new CondBlock((Data) cs[cs.length - 1], p));
        }
        return Variant.from(new CondBlock(null, (CondBlock.Case[]) cs));
    }

    @Override
    public Variant visitExprCall(GrammarParser.ExprCallContext ctx) {
        return Variant.from(new FCall(visit(ctx.getChild(0)).getData(),
                                      (Data[]) visit(ctx.getChild(1)).obj));
    }

    @Override
    public Variant visitExprInfix(GrammarParser.ExprInfixContext ctx) {
        final Data head = visit(ctx.getChild(0)).getData();
        final Variant rhs = visit(ctx.getChild(3));
        if (rhs.isData) {
            return Variant.from(new FCall(new AtomData(ctx.getChild(2).getText()),
                                          head, rhs.getData()));
        }

        final Data[] tail = (Data[]) visit(ctx.getChild(3)).obj;
        final Data[] params = new Data[tail.length + 1];
        params[0] = head;
        System.arraycopy(tail, 0, params, 1, tail.length);
        return Variant.from(new FCall(new AtomData(ctx.getChild(2).getText()), params));
    }

    @Override
    public Variant visitExprCompose(GrammarParser.ExprComposeContext ctx) {
        return Variant.from(new FCall(visit(ctx.getChild(0)).getData(), visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitExprPercent(GrammarParser.ExprPercentContext ctx) {
        return Variant.from(new FCall(new AtomData("%"), visit(ctx.getChild(0)).getData()));
    }

    @Override
    public Variant visitExprPow(GrammarParser.ExprPowContext ctx) {
        return Variant.from(new FCall(new AtomData("^"),
                                      visit(ctx.getChild(0)).getData(),
                                      visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitExprPrefix(GrammarParser.ExprPrefixContext ctx) {
        final String op = ctx.getChild(0).getText();
        final Data data = visit(ctx.getChild(1)).getData();
        return Variant.from(new FCall(new AtomData(op), data));
    }

    @Override
    public Variant visitExprMul(GrammarParser.ExprMulContext ctx) {
        return Variant.from(new FCall(new AtomData(ctx.getChild(1).getText()),
                                      visit(ctx.getChild(0)).getData(),
                                      visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitExprAdd(GrammarParser.ExprAddContext ctx) {
        return Variant.from(new FCall(new AtomData(ctx.getChild(1).getText()),
                                      visit(ctx.getChild(0)).getData(),
                                      visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitExprCons(GrammarParser.ExprConsContext ctx) {
        return Variant.from(new FCall(new AtomData(":"),
                                      visit(ctx.getChild(0)).getData(),
                                      visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitExprRel(GrammarParser.ExprRelContext ctx) {
        return Variant.from(new FCall(new AtomData(ctx.getChild(1).getText()),
                                      visit(ctx.getChild(0)).getData(),
                                      visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitExprAnd(GrammarParser.ExprAndContext ctx) {
        return Variant.from(new FCall(new AtomData("and"),
                                      visit(ctx.getChild(0)).getData(),
                                      visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitExprOr(GrammarParser.ExprOrContext ctx) {
        return Variant.from(new FCall(new AtomData("or"),
                                      visit(ctx.getChild(0)).getData(),
                                      visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitExprLazy(GrammarParser.ExprLazyContext ctx) {
        return Variant.from(new LazyExpr(visit(ctx.getChild(1)).getData()));
    }

    @Override
    public Variant visitExprSet(GrammarParser.ExprSetContext ctx) {
        return Variant.from(new SetData(ctx.getChild(1).getText().equals("="),
                                        ctx.getChild(0).getText(),
                                        visit(ctx.getChild(2)).getData()));
    }

    @Override
    public Variant visitPrimTry(GrammarParser.PrimTryContext ctx) {
        if (ctx.getChildCount() == 6) {
            return Variant.from(new TryBlock(visit(ctx.getChild(1)).getData(),
                                             visit(ctx.getChild(5)).getData(),
                                             ctx.getChild(4).getText()));
        }
        return Variant.from(new TryBlock(visit(ctx.getChild(1)).getData(),
                                         visit(ctx.getChild(3)).getData(),
                                         null));
    }

    @Override
    public Variant visitPrimF1(GrammarParser.PrimF1Context ctx) {
        if (ctx.getChildCount() == 4) {
            final Lambda.Param p = (Lambda.Param) visit(ctx.getChild(1)).obj;
            return Variant.from(new Lambda(visit(ctx.getChild(3)).getData(), true, p));
        }
        final Lambda.Param p = (Lambda.Param) visit(ctx.getChild(0)).obj;
        return Variant.from(new Lambda(visit(ctx.getChild(2)).getData(), false, p));
    }

    private boolean primF2VarargFlag = false;

    @Override
    public Variant visitPrimF2(GrammarParser.PrimF2Context ctx) {
        if (ctx.getChildCount() == 5) {
            final Lambda.Param[] p = (Lambda.Param[]) visit(ctx.getChild(1)).obj;
            return Variant.from(new Lambda(visit(ctx.getChild(4)).getData(), primF2VarargFlag, p));
        }
        return Variant.from(new Lambda(visit(ctx.getChild(3)).getData(), false));
    }
}
