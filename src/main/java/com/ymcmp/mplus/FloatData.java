/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class FloatData implements Data {

    public final double val;

    public FloatData(double val) {
        this.val = val;
    }

    @Override
    public String toSExpr() {
        return Double.toString(val);
    }

    @Override
    public String toASM() {
        return String.format("    ldc.f64 %f", val);
    }

    @Override
    public void toBytecode(final java.io.OutputStream os, final Map<String, Integer> k, MutableInt addr) throws IOException {
        if (Float.MIN_VALUE >= val && val <= Float.MAX_VALUE) {
            Utils.writeOpcode_f(os, addr, Opcodes.LDC_F32, (float) val);
            return;
        }

        Utils.writeOpcode_d(os, addr, Opcodes.LDC_F64, val);
    }
}
