/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public final class Utils {

    private Utils() {
    }

    public static int newConstant(final Map<String, Integer> k, final String val) {
        Integer boxed = k.get(val);
        if (boxed == null) {
            k.put(val, boxed = k.size());
        }
        return boxed;
    }

    public static void writeOpcode(final OutputStream os, final MutableInt addr, final byte op) throws IOException {
        os.write(op);
        addr.i++;
    }

    public static void writeOpcode_b(final OutputStream os, final MutableInt addr, final byte op, final byte i) throws IOException {
        writeOpcode(os, addr, op);
        writeOpcode(os, addr, i);
    }

    public static void writeOpcode_s(final OutputStream os, final MutableInt addr, final byte op, final short i) throws IOException {
        writeOpcode(os, addr, op);

        final ByteBuffer buf = ByteBuffer.allocate(2);
        buf.putShort(i);
        os.write(buf.array());
        addr.i += buf.capacity();
    }

    public static void writeOpcode_i(final OutputStream os, final MutableInt addr, final byte op, final int i) throws IOException {
        writeOpcode(os, addr, op);

        final ByteBuffer buf = ByteBuffer.allocate(4);
        buf.putInt(i);
        os.write(buf.array());
        addr.i += buf.capacity();
    }

    public static void writeOpcode_l(final OutputStream os, final MutableInt addr, final byte op, final long i) throws IOException {
        writeOpcode(os, addr, op);

        final ByteBuffer buf = ByteBuffer.allocate(8);
        buf.putLong(i);
        os.write(buf.array());
        addr.i += buf.capacity();
    }

    public static void writeOpcode_f(final OutputStream os, final MutableInt addr, final byte op, final float f) throws IOException {
        writeOpcode(os, addr, op);

        final ByteBuffer buf = ByteBuffer.allocate(4);
        buf.putFloat(f);
        os.write(buf.array());
        addr.i += buf.capacity();
    }

    public static void writeOpcode_d(final OutputStream os, final MutableInt addr, final byte op, final double d) throws IOException {
        writeOpcode(os, addr, op);

        final ByteBuffer buf = ByteBuffer.allocate(8);
        buf.putDouble(d);
        os.write(buf.array());
        addr.i += buf.capacity();
    }
}
