/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class SetData implements Data {

    public final boolean declareNew;
    public final String name;
    public final Data value;

    public SetData(boolean declareNew, String name, Data value) {
        this.declareNew = declareNew;
        this.name = name;
        this.value = value;
    }

    @Override
    public String toSExpr() {
        return String.format("(%s %s %s)", declareNew ? "=" : "<-", name, value.toSExpr());
    }

    @Override
    public String toASM() {
        return new StringBuilder()
                .append(value.toASM()).append("\n    ")
                .append(declareNew ? "decl" : "st").append(' ').append(name)
                .toString();
    }

    @Override
    public void toBytecode(final java.io.OutputStream os, final Map<String, Integer> k, MutableInt addr) throws IOException {
        final int slot = Utils.newConstant(k, name);
        value.toBytecode(os, k, addr);
        if (slot <= Byte.MAX_VALUE) {
            Utils.writeOpcode_b(os, addr, declareNew ? Opcodes.DECLB : Opcodes.STB, (byte) slot);
            return;
        }

        Utils.writeOpcode_i(os, addr, declareNew ? Opcodes.DECLI : Opcodes.STI, slot);
    }
}
