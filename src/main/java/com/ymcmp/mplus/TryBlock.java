/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class TryBlock implements Data {

    private static long counter = 0;

    public final Data tryClause;
    public final Data catchClause;
    public final String errHandle;

    private final long id;

    public TryBlock(Data tryClause, Data catchClause, String errHandle) {
        this.tryClause = tryClause;
        this.catchClause = catchClause;
        this.errHandle = errHandle;
        this.id = counter++;
    }

    @Override
    public String toSExpr() {
        return String.format("(try %s (%s %s))", tryClause.toSExpr(),
                             errHandle == null ? "()" : errHandle,
                             catchClause.toSExpr());
    }

    @Override
    public String toASM() {
        final StringBuilder sb = new StringBuilder();
        sb.append("    err.handle ").append('E').append(id).append('\n')
                .append(tryClause.toASM()).append('\n')
                .append("    jmp E").append(id).append("_e\n")
                .append(":E").append(id).append('\n');
        if (errHandle != null) {
            sb.append("    err.decl ").append(errHandle).append('\n');
        }
        return sb.append(catchClause.toASM()).append('\n')
                .append(":E").append(id).append("_e")
                .toString();
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, final MutableInt addr) throws IOException {
        try (final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            // Address offset is guaranteed at 1
            Utils.writeOpcode_i(baos, addr, Opcodes.ERR_HANDLE, 0); // placeholder
            tryClause.toBytecode(baos, k, addr);
            final int endJmpOffset = baos.size() + 1;
            Utils.writeOpcode_i(baos, addr, Opcodes.JMP, 0); // placeholder
            final int catchAddr = addr.i;
            if (errHandle != null) {
                Utils.writeOpcode_i(baos, addr, Opcodes.ERR_DECL, Utils.newConstant(k, errHandle));
            }
            catchClause.toBytecode(baos, k, addr);
            
            final byte[] arr = baos.toByteArray();
            final ByteBuffer buf = ByteBuffer.allocate(4);
            buf.putInt(catchAddr).get(arr, 1, buf.capacity());
            buf.clear();
            buf.putInt(addr.i).get(arr, endJmpOffset, buf.capacity());
            
            os.write(arr, 0, arr.length);
        }
    }
}
