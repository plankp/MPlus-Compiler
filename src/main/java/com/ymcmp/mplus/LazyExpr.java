/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class LazyExpr implements Data {

    private static long counter = 0;

    public final Data data;

    private final long id;

    public LazyExpr(Data data) {
        this.data = data;
        this.id = counter++;
    }

    @Override
    public String toSExpr() {
        return String.format("(lazy %s)", data.toSExpr());
    }

    @Override
    public String toASM() {
        return new StringBuilder()
                .append("    lazy.load L").append(id).append('\n')
                .append(data.toASM()).append('\n')
                .append(":L").append(id).toString();
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, final MutableInt addr) throws IOException {
        final ByteBuffer op = ByteBuffer.allocate(5);
        addr.i += op.capacity();
        try (final ByteArrayOutputStream body = new ByteArrayOutputStream()) {
            data.toBytecode(body, k, addr);
            os.write(op.put(Opcodes.LAZY_LOAD).putInt(addr.i).array());
            body.writeTo(os);
        }
    }
}
