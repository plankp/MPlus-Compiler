/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class IntData implements Data {

    public final long val;

    public IntData(long val) {
        this.val = val;
    }

    @Override
    public String toSExpr() {
        return Long.toString(val);
    }

    @Override
    public String toASM() {
        return String.format("    ldc.i64 %d", val);
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, MutableInt addr) throws IOException {
        if (Byte.MIN_VALUE >= val && val <= Byte.MAX_VALUE) {
            Utils.writeOpcode_b(os, addr, Opcodes.LDC_I8, (byte) val);
            return;
        }

        if (Short.MIN_VALUE >= val && val <= Short.MAX_VALUE) {
            Utils.writeOpcode_s(os, addr, Opcodes.LDC_I16, (short) val);
            return;
        }

        if (Integer.MIN_VALUE >= val && val <= Integer.MAX_VALUE) {
            Utils.writeOpcode_i(os, addr, Opcodes.LDC_I32, (int) val);
            return;
        }

        Utils.writeOpcode_l(os, addr, Opcodes.LDC_I64, val);
    }
}
