/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class AtomData implements Data {

    public final String val;

    public AtomData(String val) {
        this.val = val;
    }

    @Override
    public String toSExpr() {
        return val;
    }

    @Override
    public String toASM() {
        return String.format("    ld %s", val);
    }

    @Override
    public void toBytecode(final java.io.OutputStream os, final Map<String, Integer> k, MutableInt addr) throws IOException {
        switch (val) {
        case "+":
            Utils.writeOpcode(os, addr, Opcodes.LD_ADD);
            return;
        case "-":
            Utils.writeOpcode(os, addr, Opcodes.LD_SUB);
            return;
        case "*":
            Utils.writeOpcode(os, addr, Opcodes.LD_MUL);
            return;
        case "/":
            Utils.writeOpcode(os, addr, Opcodes.LD_DIV);
            return;
        case "mod":
            Utils.writeOpcode(os, addr, Opcodes.LD_MOD);
            return;
        case "^":
            Utils.writeOpcode(os, addr, Opcodes.LD_POW);
            return;
        case "%":
            Utils.writeOpcode(os, addr, Opcodes.LD_PERCENT);
            return;
        case ":":
            Utils.writeOpcode(os, addr, Opcodes.LD_CONS);
            return;
        case ">":
            Utils.writeOpcode(os, addr, Opcodes.LD_GT);
            return;
        case "<":
            Utils.writeOpcode(os, addr, Opcodes.LD_LT);
            return;
        case ">=":
            Utils.writeOpcode(os, addr, Opcodes.LD_GE);
            return;
        case "<=":
            Utils.writeOpcode(os, addr, Opcodes.LD_LE);
            return;
        case "==":
            Utils.writeOpcode(os, addr, Opcodes.LD_EQL);
            return;
        case "/=":
            Utils.writeOpcode(os, addr, Opcodes.LD_NEQ);
            return;
        case "not":
            Utils.writeOpcode(os, addr, Opcodes.LD_NOT);
            return;
        case "and":
            Utils.writeOpcode(os, addr, Opcodes.LD_AND);
            return;
        case "or":
            Utils.writeOpcode(os, addr, Opcodes.LD_OR);
            return;
        case ".":
            Utils.writeOpcode(os, addr, Opcodes.LD_FCMP);
            return;
        default:
            final int i = Utils.newConstant(k, val);
            if (i <= Byte.MAX_VALUE) {
                Utils.writeOpcode_b(os, addr, Opcodes.LDB, (byte) i);
                return;
            }
            Utils.writeOpcode_i(os, addr, Opcodes.LDI, i);
        }
    }
}
