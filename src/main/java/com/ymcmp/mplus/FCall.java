/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author YTENG
 */
public class FCall implements Data {

    public final Data base;
    public final Data[] params;

    public FCall(Data base, Data... params) {
        this.base = base;
        this.params = params;
    }

    @Override
    public String toSExpr() {
        if (params.length == 0) {
            return String.format("(%s)", base.toSExpr());
        }

        return Arrays.stream(params).map(Data::toSExpr)
                .collect(Collectors.joining(" ", String.format("(%s ", base.toSExpr()), ")"));
    }

    @Override
    public String toASM() {
        final StringBuilder sb = new StringBuilder()
                .append(base.toASM()).append('\n');
        for (final Data param : params) {
            sb.append(param.toASM()).append('\n');
        }
        return sb.append("    call ").append(params.length)
                .toString();
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, final MutableInt addr) throws IOException {
        base.toBytecode(os, k, addr);
        if (params.length == 0) {
            Utils.writeOpcode(os, addr, Opcodes.UCALL);
            return;
        }

        for (final Data param : params) {
            param.toBytecode(os, k, addr);
        }

        Utils.writeOpcode_i(os, addr, Opcodes.CALL, params.length);
    }
}
