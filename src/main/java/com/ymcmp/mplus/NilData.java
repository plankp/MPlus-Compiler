/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public final class NilData implements Data {

    public static final NilData INSTANCE = new NilData();

    private NilData() {
    }

    @Override
    public String toSExpr() {
        return "()";
    }

    @Override
    public String toASM() {
        return "    ldc.nil";
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, MutableInt addr) throws IOException {
        Utils.writeOpcode(os, addr, Opcodes.LDC_NIL);
    }
}
