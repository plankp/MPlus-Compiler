/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class Lambda implements Data {

    public static final class Param {

        public final String name;
        public final Data defaultVal;

        public Param(String name) {
            this.name = name;
            this.defaultVal = null;
        }

        public Param(String name, Data defaultVal) {
            this.name = name;
            this.defaultVal = defaultVal;
        }
    }

    private static long counter = 0;

    public final Param[] params;
    public final boolean isVarargs;
    public final Data body;

    private final long id;

    public Lambda(Data body, boolean isVarargs, Param... params) {
        this.params = params;
        this.isVarargs = isVarargs;
        this.body = body;
        this.id = counter++;
    }

    @Override
    public String toSExpr() {
        final StringBuilder sb = new StringBuilder();
        sb.append('(').append(isVarargs ? "->*" : "->").append(" (");
        for (final Param param : params) {
            if (param.defaultVal == null) {
                sb.append(param.name);
            } else {
                sb.append('(')
                        .append(param.name).append(' ')
                        .append(param.defaultVal.toSExpr()).append(')');
            }
            sb.append(' ');
        }
        if (sb.charAt(sb.length() - 1) == ' ') {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.append(") ").append(body.toSExpr()).append(')').toString();
    }

    @Override
    public String toASM() {
        final StringBuilder sb = new StringBuilder()
                .append("    func.load F").append(id).append('\n')
                .append(body.toASM()).append('\n')
                .append("    ret\n:F").append(id);
        for (final Param param : params) {
            sb.append("\n    dup\n");
            if (param.defaultVal == null) {
                sb.append("    func.map ").append(param.name);
            } else {
                sb.append(param.defaultVal.toASM())
                        .append("\n    func.omap ").append(param.name);
            }
        }
        if (isVarargs) {
            sb.append("\n    dup\n    func.varargs");
        }
        return sb.toString();
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, final MutableInt addr) throws IOException {
        final ByteBuffer op = ByteBuffer.allocate(5);
        addr.i += op.capacity();
        try (final ByteArrayOutputStream bodyOs = new ByteArrayOutputStream()) {
            body.toBytecode(bodyOs, k, addr);
            os.write(op.put(Opcodes.FUNC_LOAD).putInt(addr.i).array());
            bodyOs.writeTo(os);
        }

        for (final Param param : params) {
            Utils.writeOpcode(os, addr, Opcodes.DUP);
            if (param.defaultVal == null) {
                Utils.writeOpcode_i(os, addr, Opcodes.FUNC_MAP, Utils.newConstant(k, param.name));
            } else {
                param.defaultVal.toBytecode(os, k, addr);
                Utils.writeOpcode_i(os, addr, Opcodes.FUNC_OMAP, Utils.newConstant(k, param.name));
            }
        }
        if (isVarargs) {
            Utils.writeOpcode(os, addr, Opcodes.DUP);
            Utils.writeOpcode(os, addr, Opcodes.FUNC_VARARGS);
        }
    }
}
