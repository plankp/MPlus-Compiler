/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.IOException;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public interface Data {

    public String toSExpr();

    public String toASM();

    public void toBytecode(final java.io.OutputStream os, final Map<String, Integer> k, MutableInt addr) throws IOException;
}
