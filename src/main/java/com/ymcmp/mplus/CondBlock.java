/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymcmp.mplus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author YTENG
 */
public class CondBlock implements Data {

    public static class Case {

        public final Data condition;
        public final Data body;

        public Case(Data condition, Data body) {
            this.condition = condition;
            this.body = body;
        }
    }

    private static long counter = 0;

    public final Case[] cases;
    public final Data elseCase;

    private final long id;

    public CondBlock(Data elseCase, Case... cases) {
        this.cases = cases;
        this.elseCase = elseCase;
        this.id = counter++;
    }

    @Override
    public String toSExpr() {
        final StringBuilder sb = new StringBuilder();
        sb.append("(cond");
        for (final Case c : cases) {
            sb.append(" (")
                    .append(c.condition.toSExpr()).append(' ')
                    .append(c.body.toSExpr()).append(')');
        }
        if (elseCase != null) {
            sb.append(" (else ").append(elseCase.toSExpr()).append(')');
        }
        return sb.append(')').toString();
    }

    @Override
    public String toASM() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < cases.length; ++i) {
            sb.append(cases[i].condition.toASM()).append('\n')
                    .append("    jmp.true C").append(id).append('_').append(i).append('\n');
        }
        if (elseCase == null) {
            // Else clause is not present, throw error if fallthrough
            sb.append("    ldc.str @\"Cases were not handled\"\nerr.throw\n");
        } else {
            // Else clause is present, guaranteed returns a value
            sb.append(elseCase.toASM()).append('\n')
                    .append("    jmp C").append(id).append("_e\n");
        }
        for (int i = 0; i < cases.length; ++i) {
            sb.append(":C").append(id).append('_').append(i).append('\n')
                    .append(cases[i].body.toASM()).append('\n');

            // Last case does not need the jump (since it is already in position
            if (i != cases.length - 1) {
                sb.append("    jmp C").append(id).append("_e\n");
            }
        }
        return sb.append(":C").append(id).append("_e").toString();
    }

    @Override
    public void toBytecode(final OutputStream os, final Map<String, Integer> k, final MutableInt addr) throws IOException {
        try (final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            final int[] caseHeadJmpOffset = new int[cases.length];
            final int[] caseTailJmpOffset = new int[cases.length - 1];
            final int[] caseHeadJmpAddr = new int[cases.length];
            int elseJmpOffset = -1;

            for (int i = 0; i < cases.length; ++i) {
                cases[i].condition.toBytecode(baos, k, addr);
                caseHeadJmpOffset[i] = baos.size() + 1;
                Utils.writeOpcode_i(baos, addr, Opcodes.JMP_TRUE, 0); // placeholder
            }

            if (elseCase == null) { // throw error on fallthrough
                Utils.writeOpcode_i(baos, addr, Opcodes.LDC_STR, Utils.newConstant(k, "Cases were not handled"));
                Utils.writeOpcode(baos, addr, Opcodes.ERR_THROW);
            } else {
                elseCase.toBytecode(baos, k, addr);
                elseJmpOffset = baos.size() + 1;
                Utils.writeOpcode_i(baos, addr, Opcodes.JMP, 0); // placeholder
            }

            for (int i = 0; i < cases.length; ++i) {
                // Store the actual address
                caseHeadJmpAddr[i] = addr.i;
                cases[i].body.toBytecode(baos, k, addr);

                // Last case does not need the jump (since it is already in position
                if (i != cases.length - 1) {
                    caseTailJmpOffset[i] = baos.size() + 1;
                    Utils.writeOpcode_i(baos, addr, Opcodes.JMP, 0); // placeholder
                }
            }

            final byte[] arr = baos.toByteArray();
            final ByteBuffer buf = ByteBuffer.allocate(4);

            // Fill in head jmp addresses
            for (int i = 0; i < caseHeadJmpOffset.length; ++i) {
                buf.clear();
                buf.putInt(caseHeadJmpAddr[i]).get(arr, caseHeadJmpOffset[i], buf.capacity());
            }

            buf.clear();
            buf.putInt(addr.i);

            // Fill in else jmp address if needed
            if (elseJmpOffset > -1) {
                buf.get(arr, elseJmpOffset, buf.capacity());
            }

            // Fill in tail jmp addresses
            for (int i = 0; i < caseTailJmpOffset.length; ++i) {
                buf.rewind();
                buf.get(arr, caseTailJmpOffset[i], buf.capacity());
            }

            // Write to the external stream
            os.write(arr, 0, arr.length);
        }
    }
}
