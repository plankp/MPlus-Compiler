grammar Grammar;

P_LCURL: '{';
P_RCURL: '}';
P_LSQUARE: '[';
P_RSQUARE: ']';
P_LPAREN: '(';
P_RPAREN: ')';

WHITESPACE
  : [ \t\r\n] -> skip
  ;

COMMENT
  : '#' ~[\r\n]* -> skip
  ;

K_OR: 'or';
K_AND: 'and';
K_MOD: 'mod';
K_NOT: 'not';
K_IF: 'if';
K_ELSE: 'else';
K_DO: 'do';
K_END: 'end';
K_LAZY: 'lazy';
K_TRY: 'try';
K_CATCH: 'catch';

Y_LE: '<=';
Y_GE: '>=';
Y_EQL: '==';
Y_DECL
  : '<-'
  | '='
  ;

Y_FUNC: '->';
Y_NEQ: '/=';
Y_LT: '<';
Y_GT: '>';
Y_SEMI: ';';
Y_COLON: ':';
Y_COMMA: ',';
Y_ADD: '+';
Y_SUB: '-';
Y_MUL: '*';
Y_DIV: '/';
Y_POW: '^';
Y_PERCENT: '%';
Y_INFIX: '~';

L_NUMBER
  : '0b' [01]+
  | '0c' [0-7]+
  | '0d' [0-9]+
  | '0x' [0-9a-fA-F]+
  | ('0' | [1-9][0-9]*)('.' [0-9]+)?
  ;

Y_PERIOD: '.';

L_ATOM
  : '@' L_IDENT
  | '@"' .*? '"'
  ;

L_IDENT
  : [a-zA-Z_'][a-zA-Z0-9_']* [?!]?
  ;

exprs
  : expr (Y_SEMI expr)* Y_SEMI?
  ;

innerArgs
  : expr (Y_COMMA expr)*
  ;

args
  : P_LPAREN innerArgs? P_RPAREN
  ;

expr
  : primary #delegatePrim
  | expr args #exprCall
  | expr Y_INFIX L_IDENT (expr | args) #exprInfix
  | <assoc=right> expr Y_PERIOD expr #exprCompose
  | expr Y_PERCENT #exprPercent
  | <assoc=right> expr Y_POW expr #exprPow
  | (Y_ADD | Y_SUB | K_NOT) expr #exprPrefix
  | expr (Y_MUL | Y_DIV | K_MOD) expr #exprMul
  | expr (Y_ADD | Y_SUB) expr #exprAdd
  | <assoc=right> expr Y_COLON expr #exprCons
  | expr (Y_LT | Y_GT | Y_LE | Y_GE | Y_EQL | Y_NEQ) expr #exprRel
  | expr K_AND expr #exprAnd
  | expr K_OR expr #exprOr
  | K_LAZY expr #exprLazy
  | <assoc=right> L_IDENT Y_DECL expr #exprSet
  ;

primary
  : P_LPAREN P_RPAREN #primNil
  | P_LPAREN expr P_RPAREN #primBraced
  | P_LPAREN params? P_RPAREN Y_FUNC expr #primF2
  | K_TRY expr K_CATCH (Y_FUNC L_IDENT)? expr #primTry
  | K_DO exprs K_END #primDelegateDo
  | P_LCURL cases P_RCURL #primCond
  | (L_NUMBER | L_ATOM | L_IDENT) #primVal
  | Y_MUL? param Y_FUNC expr #primF1
  | P_LSQUARE innerArgs? P_RSQUARE #primArray
  ;

params
  : Y_MUL? param
  | param Y_COMMA params
  ;

param
  : L_IDENT P_LSQUARE expr P_RSQUARE
  | L_IDENT
  ;

cases
  : expr K_ELSE
  | expr K_IF expr
  | expr K_IF expr Y_COMMA cases
  ;